var Advent = {
	FileLoader : {
		LoadFile: function(filename, cbFn)
		{
			var xhr = new XMLHttpRequest();
			xhr.open("get", filename, true);
			if ( ! xhr["onload"] )
			{
		
				xhr.onreadystatechange = this.onLoadFile_readyStateChange.bind(this, xhr, cbFn);
			} else {			
				xhr.onload = this.onLoadFile.bind(this, xhr, cbFn);
			}
			xhr.send([]);	
		},
		onLoadFile: function(resp, cbFn)
		{
			if ( typeof cbFn === "function")
				cbFn(resp);
			else if ( typeof cbFn === "object") {
				if ( cbFn["innerHTML"])
					cbFn.innerHTML = resp.responseText;
				else if ( cbFn.type )
					cbFn.value = resp.responseText;
			}
		},
		onLoadFile_readyStateChange: function(resp, cbFn)
		{
			if ( resp.readyState == 4)
			{
				if ( resp.status == 200 || resp.status == 304)
				{
					this.onLoadFile(resp, cbFn);
				}
			}
		}
	},
	setupErrorHandler: function()
	{
		window.addEventListener("error", this.onError.bind(this));

	},
	onError: function(messageOrEvent, source, lineno, colno, error)
	{
		alert("error occurred")
		var message = "";
		
		if ( messageOrEvent instanceof ErrorEvent)
		{
			var err = new Error(messageOrEvent);
message = [
            'Message: ' + messageOrEvent.message,
            'URL: ' + messageOrEvent.filename,
            'Line: ' + messageOrEvent.lineno,
            'Column: ' + messageOrEvent.colno,
            'Error object: ' + JSON.stringify(messageOrEvent.error),
			'Stack: ' + err.stack
        ].join(' - ');

		} else if ( typeof messageOrEvent === "object")
		{
			 var stack = messageOrEvent.stack;
			message = messageOrEvent.toString();
			if (stack) {
				message += '\n' + stack;
			}
			//message = JSON.stringify(messageOrEvent);
		} else 
 message = [
            'Message: ' + messageOrEvent,
            'URL: ' + source,
            'Line: ' + lineno,
            'Column: ' + colno,
            'Error object: ' + JSON.stringify(error)
        ].join(' - ');
		alert(message);
	},
	LoadReadMe: function()
	{
		this.FileLoader.LoadFile("README.txt", document.querySelector("pre"));
	},
	
	LoadDefaultInput: function()
	{
		this.FileLoader.LoadFile("Defaultinput.txt", document.getElementById("inp"));
	},
	
	LoadInput: function()
	{
		this.FileLoader.LoadFile("input.txt", document.getElementById("inp"));
		
	},
	
	trim: function(strIn)
	{
		return strIn.replace(/^\s+|\s+$/g, '');
	},
	
	GetInput: function()
	{
		return document.getElementById("inp").value;
	},
	WriteOutput: function(output)
	{
		var res = document.getElementById("res");
		if ( output instanceof Array)
			res.value = output.join("\n");
		else
			res.value =output;
	},
	
	objSize: function(obj)
	{
		var size = 0, key;
		for (key in obj) {
			if (obj.hasOwnProperty(key)) size++;
		}
		return size;

	},
	extend: function ()
	{
		 // Variables
		var extended = {};
		var deep = false;
		var i = 0;
		var length = arguments.length;
		
		// Check if a deep merge
		if ( Object.prototype.toString.call( arguments[0] ) === '[object Boolean]' ) {
			deep = arguments[0];
			i++;
		}
		
		// Merge the object into the extended object
		var merge = function (obj) {
			for ( var prop in obj ) {
				if ( Object.prototype.hasOwnProperty.call( obj, prop ) ) {
					// If deep merge and property is an object, merge properties
					if ( deep && Object.prototype.toString.call(obj[prop]) === '[object Object]' ) {
						extended[prop] = this.extend( true, extended[prop], obj[prop] );
					} else {
						extended[prop] = obj[prop];
					}
				}
			}
		};
		
		// Loop through each object and conduct a merge
		for ( ; i < length; i++ ) {
			var obj = arguments[i];
			merge(obj);
		}
		
		return extended;
	},
	Part1: function()
	{
		
			
		var vals = this.GetInput().split("\n");
		var outStr = [];
		if ( this["part1Reduction"])
		{
			var calcs = vals.map(this.performPart1.bind(this));
		
			var solution = calcs.reduce(this.part1Reduction.bind(this), 0);
		
			outStr.push(solution);
		} else {
			outStr = vals.reduce(this.performPart1.bind(this), []);
		}
		 
		
		
		outStr.push("end");
		this.WriteOutput(outStr);
	
	},
	Part2: function()
	{
		var vals = this.GetInput().split("\n");
		var outStr = [];
	
		if ( this["part2Reduction"])
		{
			var calcs = vals.map(this.performPart2.bind(this));
		
			var solution = calcs.reduce(this.part2Reduction.bind(this), 0);
		
			outStr.push(solution);
		} else {
			outStr = vals.reduce(this.performPart2.bind(this), []);
		}
		
		outStr.push("end");
		this.WriteOutput(outStr);
	}

}

//Advent.setupErrorHandler();
try {

 	module.exports = Advent;

} catch (e) {

	Advent.LoadReadMe();
	Advent.LoadDefaultInput();
}

